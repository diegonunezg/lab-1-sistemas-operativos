#include <stdio.h>
#include <unistd.h>



int main() {
    // Este ciclo for hace el llamado de la función fork() 3 veces, creando 3 procesos hijos.
    for (int n = 1; n <= 3; n++) {
        // El proceso padre crea un hijo.
        int pid = fork();

        // Si el pid es 0, significa que el proceso ejecutandose es un hijo.
        if (pid == 0) {
            printf("\tH%d creado!\n", n);
            // El proceso hijo termina, por lo que no continúa con el ciclo. Así no crea más hijos.
            return 0;
        }
    }

    return 0;
}