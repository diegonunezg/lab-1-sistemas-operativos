#include <stdio.h>
#include <unistd.h>

int main() {

    /* En una priera instancia, el proceso en ejecución crea otro, duplicándose.
       En la siguiente iteración del ciclo, estos 2 procesos crean hijos, duplicándose nuevamente.
       En cada iteración ocurre una duplicación, esto se puede expresar matemáticamente como p = 2^n
       donde "p" es la cantidad final de procesos, y "n" es el numero de iteraciones. 
       Por lo que si queremos 8 procesos, hay que hacer 3 iteraciones.
    */
    for (int n = 1; n <= 3; n++) {
        int pid = fork();
    }

    printf("Hola me presento, soy el proceso %d\n", getpid());

    return 0;
}