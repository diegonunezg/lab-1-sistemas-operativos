# Lab 1 Sistemas Operativos



## Resolución del primer laboratorio del ramo Sistemas Operativos y Redes

Los ejercicios son los siguientes:

1.	Implementar solución en C Linux que mediante uso de la función fork() el programa principal y sus hijos restantes creen exactamente 8 procesos en total, donde cada proceso muestra su PID.



2.	Implementar solución en C Linux que mediante uso de la función fork() el programa principal cree exactamente 3 procesos hijos H1, H2 y H3 (los hijos no creen procesos), donde cada proceso hijo se identifica al momento de ser creado (envía un mensaje tal como “H1 creado!”.



3.	Implementar solución en C Linux que mediante uso de la función fork() el programa principal cree un proceso hijo H1 el cual crea un proceso hijo H2, y H2 crea otro proceso hijo H3. Cada proceso hijo se identifica al momento de ser creado y al momento de terminar su ejecución. Por ejemplo, “H1 creado” y “H1 termina”.

