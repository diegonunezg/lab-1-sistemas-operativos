#include <stdio.h>
#include <unistd.h>


int main() {
    // Padre crea su primer hijo.
    int pid = fork();
    
    // Si el pid es 0, significa que el proceso ejecutandose es un hijo.
    if (pid == 0) {
        printf("\tH1 creado\n");

        // El primer hijo crea un segundo.
        pid = fork();

        // Si el pid es 0, el proceso en ejecución es un hijo que no tiene hijos.
        if (pid == 0) {
            printf("\tH2 creado\n");

            // El segundo hijo crea un tercero.
            pid = fork();
            if (pid == 0) {
                printf("\tH3 creado\n");
                printf("\tH3 termina\n");
            } else {
                printf("\tH2 termina\n");
            }

        } else {
            printf("\tH1 termina\n");
        }

    }

    return 0;
}